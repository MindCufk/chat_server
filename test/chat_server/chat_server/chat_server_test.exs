defmodule ChatServer.ChatServerTest do
  use ChatServer.DataCase

  alias ChatServer.ChatServer

  describe "rooms" do
    alias ChatServer.ChatServer.Room

    @valid_attrs %{max_users: 42, password: "some password", room_id: 42}
    @update_attrs %{max_users: 43, password: "some updated password", room_id: 43}
    @invalid_attrs %{max_users: nil, password: nil, room_id: nil}

    def room_fixture(attrs \\ %{}) do
      {:ok, room} =
        attrs
        |> Enum.into(@valid_attrs)
        |> ChatServer.create_room()

      room
    end

    test "list_rooms/0 returns all rooms" do
      room = room_fixture()
      assert ChatServer.list_rooms() == [room]
    end

    test "get_room!/1 returns the room with given id" do
      room = room_fixture()
      assert ChatServer.get_room!(room.id) == room
    end

    test "create_room/1 with valid data creates a room" do
      assert {:ok, %Room{} = room} = ChatServer.create_room(@valid_attrs)
      assert room.max_users == 42
      assert room.password == "some password"
      assert room.room_id == 42
    end

    test "create_room/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = ChatServer.create_room(@invalid_attrs)
    end

    test "update_room/2 with valid data updates the room" do
      room = room_fixture()
      assert {:ok, room} = ChatServer.update_room(room, @update_attrs)
      assert %Room{} = room
      assert room.max_users == 43
      assert room.password == "some updated password"
      assert room.room_id == 43
    end

    test "update_room/2 with invalid data returns error changeset" do
      room = room_fixture()
      assert {:error, %Ecto.Changeset{}} = ChatServer.update_room(room, @invalid_attrs)
      assert room == ChatServer.get_room!(room.id)
    end

    test "delete_room/1 deletes the room" do
      room = room_fixture()
      assert {:ok, %Room{}} = ChatServer.delete_room(room)
      assert_raise Ecto.NoResultsError, fn -> ChatServer.get_room!(room.id) end
    end

    test "change_room/1 returns a room changeset" do
      room = room_fixture()
      assert %Ecto.Changeset{} = ChatServer.change_room(room)
    end
  end
end
