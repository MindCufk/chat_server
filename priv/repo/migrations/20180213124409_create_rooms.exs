defmodule ChatServer.Repo.Migrations.CreateRooms do
  use Ecto.Migration

  def change do
    create table(:rooms) do
      add :room_id, :integer
      add :password, :string
      add :max_users, :integer

      timestamps()
    end

  end
end
