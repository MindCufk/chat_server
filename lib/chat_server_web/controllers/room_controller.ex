defmodule ChatServerWeb.RoomController do
  use ChatServerWeb, :controller

  alias ChatServer.ChatServer
  alias ChatServer.ChatServer.Room

  action_fallback ChatServerWeb.FallbackController

  def create(conn, %{"room" => room_params}) do
    with {:ok, %Room{} = room} <- ChatServer.create_room(room_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", room_path(conn, :show, room))
      |> render("show.json", room: room)
    end
  end

  def update(conn, %{"id" => id, "password" => password, "room" => room_params}) do
    room = Repo.get_by!(Room, id: id, password: password)

    with {:ok, %Room{} = room} <- ChatServer.update_room(room, room_params) do
      render(conn, "show.json", room: room)
    end
  end

  def delete(conn, %{"id" => id, "password" => password}) do
    room = Repo.get_by!(Room, id: id, password: password)
    
    with {:ok, %Room{}} <- ChatServer.delete_room(room) do
      send_resp(conn, :no_content, "")
    end
  end
end
