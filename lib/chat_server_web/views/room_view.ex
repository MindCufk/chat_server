defmodule ChatServerWeb.RoomView do
  use ChatServerWeb, :view
  alias ChatServerWeb.RoomView

  def render("index.json", %{rooms: rooms}) do
    %{data: render_many(rooms, RoomView, "room.json")}
  end

  def render("show.json", %{room: room}) do
    %{data: render_one(room, RoomView, "room.json")}
  end

  def render("room.json", %{room: room}) do
    %{id: room.id,
      room_id: room.room_id,
      password: room.password,
      max_users: room.max_users}
  end
end
