defmodule ChatServer.ChatServer.Room do
  use Ecto.Schema
  import Ecto.Changeset
  alias ChatServer.Room


  schema "rooms" do
    field :max_users, :integer
    field :password, :string
    field :room_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%Room{} = room, attrs) do
    room
    |> cast(attrs, [:room_id, :password, :max_users])
    |> validate_required([:room_id, :password, :max_users])
  end
end
